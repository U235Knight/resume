# Resume Generator

This repository contains tools and formatting rules for generating a resume in
PDF format from asciidoc-formatted sources. It makes use of Asciidoctor and
Asciidoctor-PDF (an extension) to create the final output.

Check out [Asciidoctor](https://asciidoctor.org/), it's an awesome project!

## Usage

To generate your resume, you must first ensure that you have formatted it
properly. Once this is done, simply run the generation script:

---
# ./gen-resume.sh -r \<resume-dir\> -o my-resume.pdf
---
