:nofooter:

include::{resume-top}/contact.adoc[]

= {name}

[cols="^.^5,^.^1,^.^2,^.^1,^.^5", width=75%, align=center, padding=0]
|===
| [.deets]#{email}# | - | [.deets]#{phone}# | - | [.deets]#{website}#
|===

{empty}

== Work Experience

'''

include::{resume-top}/experience.adoc[]

== Project Highlights

'''

include::{resume-top}/projects.adoc[]

{empty}

== Education

'''

include::{resume-top}/education.adoc[]

[cols="<.^5,>.^2", align=center]
|===
| *{major}* | {school-years}
| {school} - {school-loc}   |
|===

{empty}

== Skills

'''

include::{resume-top}/skills.adoc[]
