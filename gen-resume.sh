#!/bin/bash
###############################################################################
#
# FILENAME: gen-resume.sh
# BRIEF:
# Runs the necessary asciidoctor command to generate the resume provided by
# argument $1. The resume format is dictated by the contents of the file:
# meta/base.adoc. This script does a check to make sure that all the required
# files are present in the directory passed to it before attempting to make the
# conversion.
#
###############################################################################

# It's possible that this script will be run outside of what pwd would return
WD=$( dirname "$(realpath "$0")" )
META_DIR="$WD"/meta
SCHEMA_FILE="$META_DIR"/file-list.txt

usage() {
	echo "Usage: gen-resume.sh -r <dir> [ -o <name> | h]"
	echo "Generates a resume in PDF format given a provided input directory"
	echo "containing Asciidoc source"
	echo "args:"
	echo "-r <dir>          (Required) Input directory"
	echo "-o <name>         (Optional) Output filename. Default is the same"
	echo "                             as the input dir"
	echo "-h                (Optional) Print this message"
}

validate_schema() {
	while IFS="" read -r line ; do
		if ! [ -e "$1"/"$line" ] ; then
			return 1
		fi
	done < "$SCHEMA_FILE"
	return 0
}

while getopts "r:o:h" opt ; do
	case "$opt" in
		r)
			if ! [ -d "$OPTARG" ] ; then
				echo "Cannot find directory: ${OPTARG}"
				exit 1
			fi
			resume_dir="$OPTARG"
			outfile=$(basename "$OPTARG")
			;;
		o)
			outfile="$OPTARG"
			;;
		h)
			usage
			exit 0
			;;
		?)
			echo "Invalid option ${OPTARG}"
			usage
			;;
	esac
done

if [ "$resume_dir" == "" ] || ! [ -d "$resume_dir" ] ; then
	echo "Must provide a valid resume dir path"
	usage
	exit 1
fi

if ! validate_schema "$resume_dir" ; then
	echo "Directory ${resume_dir} does not conform to the expected schema."
	echo "The following files must be present in the provided dir:"
	cat "$SCHEMA_FILE"
	exit 1
fi

asciidoctor-pdf	-r "$META_DIR"/themes/ext/xp.rb \
				-a pdf-themesdir="$META_DIR"/themes \
				-a pdf-theme=resume \
				-a pdf-fontsdir="$META_DIR"/themes/fonts \
				-a resume-top="$WD"/"$resume_dir" \
				"$META_DIR"/base.adoc -o "$outfile".pdf
